package com.bncc;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int numberOfRows = 0;
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter number of rows: ");
        numberOfRows = userInput.nextInt();
        System.out.println("Printing triangle 1 with "+ numberOfRows + " number of rows");
        for(int i=0; i<numberOfRows; i++){
            for(int j=0; j<=i; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
        System.out.println("\nPrinting triangle 2 with "+ numberOfRows + " number of rows");
        for(int i=0; i<numberOfRows; i++){
            for(int j=0; j<numberOfRows; j++){
                if(i>j){
                    System.out.print(" ");
                }else{
                    System.out.print("*");
                }
            }
            System.out.println("");
        }
    }
}
